import React from 'react';
import './App.css';
import {getLocation} from './getLocations'
import {Weather} from './components/Weather';

const API_URL = 'https://api.openweathermap.org/data/2.5'
const API_KEY = 'a4c49575b292fe4ad6a8e2056dea937a';

class App extends React.Component {
  state = {
    lon: '',
    lat: '',
    query: '',
    data: undefined
  };

  async componentDidMount() {
    const { coords } = await getLocation();
    console.log(coords);
    this.setState(state => ({
      ...state,
      lat: coords.latitude,
      lon: coords.longitude
    }));
    this.fetchWeather();
  }

  fetchWeather = async () => {
    const { lat, lon } = this.state;
    const response = await fetch(
      `${API_URL}/weather?lat=${lat}&lon=${lon}&appid=${API_KEY}&units=metric`
    );
    const data = await response.json();
    this.setCurrentWeather(data);
  };

  setCurrentWeather = data => {
    const {
      main: { temp, feels_like },
      wind: { speed: wind_speed, deg: wind_degree },
      sys: { country, sunrise, sunset },
      name,
      weather
    } = data;
    this.setState(state => {
      return {
        ...state,
        data: {
          temp,
          feels_like,
          wind_speed,
          wind_degree,
          country,
          sunrise,
          sunset,
          name,
          weather
        }
      };
    });
  };

  renderContent() {
    return this.state.data ? (
      <>
        <Weather {...this.state.data} />
      </>
    ) : (
      'Loading...'
    );
  }

  render() {
    return (
      <div className='AppContainer'>
        <div className='AppContant'>{this.renderContent()}</div>
      </div>
    );
  }
}

export default App;
